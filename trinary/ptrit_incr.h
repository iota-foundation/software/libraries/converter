#ifndef __BCT_INCREMENT_H
#define __BCT_INCREMENT_H
#include "trinary/ptrit.h"

void ptrit_increment(ptrit_t *const, size_t, size_t);

#endif  //__BCT_INCREMENT_H
