#ifndef __TRIT_PTRIT_H__
#define __TRIT_PTRIT_H__

#include "trinary/ptrit_incr.h"
#include "trinary/stdint.h"
#include "trinary/trits.h"

void trits_to_ptrits(trit_t*, ptrit_t*, size_t);
void ptrits_to_trits(ptrit_t*, trit_t*, size_t, size_t);

#endif  //__TRIT_PTRIT_H__
