#ifndef __PTRIT_H__
#define __PTRIT_H__

#define HIGH_BITS 0xFFFFFFFFFFFFFFFF
#define LOW_BITS 0x0000000000000000
#include "trinary/stdint.h"

typedef int64_t ptrit_s;

typedef struct {
  ptrit_s low;
  ptrit_s high;
} ptrit_t;

#endif  //__PTRIT_H__
