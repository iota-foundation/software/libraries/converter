#ifndef __BCT_H__
#define __BCT_H__

//#include <iota-types.h>
#include <stdint.h>
#include <unistd.h>
#include "trinary/trits.h"

typedef uint8_t bct_t;

void copy_bct(bct_t *, size_t, bct_t *, size_t, size_t);
void copy_bct_to_trits(trit_t *t, bct_t *s, size_t i, size_t l);
void write_trit(bct_t *, int, trit_t);
trit_t get_trit(bct_t *data, int index);

#endif  //__BCT_H__
