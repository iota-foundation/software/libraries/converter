#ifndef __TRIT_TRYTE_H__
#define __TRIT_TRYTE_H__

#include "trinary/stdint.h"
#include "trinary/trits.h"
#include "trinary/tryte.h"

void trits_to_trytes(trit_t*, tryte_t*, size_t);
void trytes_to_trits(tryte_t*, trit_t*, size_t);

#endif  //__TRIT_TRYTE_H__
