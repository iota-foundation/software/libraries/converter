#ifndef __BYTE_H__
#define __BYTE_H__

#include "trinary/stdint.h"

typedef int8_t byte_t;

#endif  //__BYTE_H__
